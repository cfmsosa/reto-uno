<?php

Route::get('/', 'Auth\LoginController@showlogin')->middleware('guest');
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/usuarios', 'UserController@index')->name('usuarios')->middleware('auth');

Route::get('/usuarios/{id}', 'UserController@show' )->where('id','[0-9]+')->name('showusuarios')->middleware('auth');

Route::get('/usuarios/nuevo', 'UserController@create')->name('createusuarios')->middleware('guest');
Route::post('/usuarios/set', 'UserController@set')->name('setusuarios');

Route::get('/categorias', 'ProductoController@showCategorias')->name('showcategorias');

Route::get('/categorias/nuevo', 'ProductoController@create')->name('createcategorias');
Route::post('/categorias/set', 'ProductoController@set')->name('setcategorias');
Route::get('/categorias/{categoria}/editar', 'ProductoController@edit')->name('editcategorias');
Route::put('/categorias/{categoria}', 'ProductoController@update')->name('updatecategorias');

Route::get('/categorias/{id}', 'ProductoController@showProductos')->where('id','[0-9]+')->name('showproductos');

Route::get('/productos/{id}', 'ProductoController@singleProductos')->where('id','[0-9]+')->name('singleproductos');
Route::get('/productos/nuevo', 'ProductoController@createproducto')->name('createproductos');
Route::post('/productos/set', 'ProductoController@setproductos')->name('setproductos');
Route::get('/productos/{producto}/editar', 'ProductoController@editproducto')->name('editproductos');
Route::put('/productos/{producto}', 'ProductoController@updateproducto')->name('updateproductos');

Route::get('/buscar/', 'ProductoController@filterProductos')->name('filterproductos');