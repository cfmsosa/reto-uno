<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\Http\Controllers\Controller;


class LoginController extends Controller
{
    public function showlogin(){

        return view('login');
    }

    public function login(){
        $valid = $this->validate(request(),[
            'email' => 'email|required',
            'password' => 'required'
        ],[
            'email.email' => 'Correo invalido',
            'email.required' => 'Ingresa el email',
            'password.required' => 'Contraseña obligatoria'
        ]);

        if (Auth::attempt($valid)) {   
            return redirect()->route('home');

        } 
            return back()
            ->withInput(request(['email']))
            ->withErrors(['email' => 'Datos incorrectos']);

    }

    public function logout(){

        Auth::logout();
        return redirect('/');
    }
}
