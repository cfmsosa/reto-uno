<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{

	public function index() {
		$users = User::all();
		return view('users.index', compact('users'));
	}

	public function show($id) {
		$user = User::find($id);
		return view('users.show', compact('user'));
	}

	public function create() {
		return view('users.create');
	}

	public function set() {
		$data = request()->validate([
			'name' => 'required',
			'email' => ['required','email','unique:users,email'],
			'password' => 'required|min:6'
		],[
			'name.required' => 'Nombre obligatorio',
			'email.required' => 'Email obligatorio',
			'email.email' => 'Email Invalido',
			'email.unique' => 'Email ya existe',
			'password.required' => 'Contraseña obligatorio',
			'password.min' => 'Contraseña demasiado corta'
		]);

		User::create([
			'name' => $data['name'],
			'email' => $data['email'],
			'password' => bcrypt($data['password'])
		]);

		return redirect()->route('usuarios');
	}

}
