<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use App\Producto;

class ProductoController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function showCategorias() {
    	$categorias = Categoria::all();
        return view('productos.showcategorias', compact('categorias'));
    }

    public function showProductos($id) {

        $productos = Producto::all()->where('categorias_id', $id );
        $categoria = Categoria::find( $id );
        return view('productos.showproductos', compact('productos' , 'categoria'));
    }

    public function filterProductos() {
        $data = request()->validate([
            'campo' => 'required'
        ]);

        $productos = Producto::where('nombre', 'LIKE' , '%'.$data['campo'].'%' )->get();

    	return view('productos.filterproductos', compact('productos'));
    }

    public function singleProductos($id) {
        $producto = Producto::find($id);
        $categoria = Categoria::find( $producto->categorias_id );
        return view('productos.singleproductos', compact('producto', 'categoria'));
    }

    public function create() {
        return view('productos.createcategorias');
    }

    public function set() {
        $data = request()->validate([
            'nombre' => ['required','unique:categorias,nombre']
        ],[
            'nombre.required' => 'Nombre obligatorio',
            'nombre.unique' => 'Categoria existente',

        ]);

        Categoria::create([
            'nombre' => $data['nombre']
        ]);

        return redirect()->route('showcategorias');
    }

    public function edit(Categoria $categoria) {
        return view('productos.editcategorias',['categoria' => $categoria]);
    }

    public function update(Categoria $categoria) {
        $data = request()->validate([
            'nombre' => ['required','unique:categorias,nombre']
        ],[
            'nombre.required' => 'Nombre obligatorio',
            'nombre.unique' => 'Categoria existente',

        ]); 

        $categoria->update($data);
        return redirect()->route('showcategorias');
    }

        public function createproducto() {
        $categorias = Categoria::all();
        return view('productos.createproductos', compact('categorias'));
    }

    public function setproductos() {

        $data = request()->validate([
            'nombre' => ['required','unique:productos,nombre'],
            'categorias_id' => 'required',
            'precio' => 'required',
            'descripcion' => ''
        ],[
            'nombre.required' => 'Nombre obligatorio',
            'nombre.unique' => 'Nombre existente',
            'categorias_id.required' => 'Categoria obligatoria',
            'precio.required' => 'Precio obligatorio'
        ]);

        $nuevo = Producto::create([
            'nombre' => $data['nombre'],
            'categorias_id' => $data['categorias_id'] ,
            'precio' => $data['precio'],
            'descripcion' => $data['descripcion'],
        ]);

        return redirect()->route('singleproductos' , ['id' => $nuevo->id]);
    }

        public function editproducto(Producto $producto) {
        $categorias = Categoria::all();
        return view('productos.editproductos',['producto' => $producto ,'categorias' => $categorias ]);
    }

    public function updateproducto(Producto $producto) {
        $data = request()->validate([
            'nombre' => 'required',
            'categorias_id' => 'required',
            'precio' => 'required',
            'descripcion' => ''
        ],[
            'nombre.required' => 'Nombre obligatorio',
            'categorias_id.required' => 'Categoria obligatoria',
            'precio.required' => 'Precio obligatorio'
        ]);
        $producto->update($data);
        return redirect()->route('singleproductos' , ['id' => $producto->id]);
    }
}
