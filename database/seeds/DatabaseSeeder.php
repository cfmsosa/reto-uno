<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->clearTables(['users','categorias','productos']);

        $this->call(usersSeeder::class);
        $this->call(categoriasSeeder::class);
        $this->call(productosSeeder::class);
    }

    protected function clearTables(array $tables) {
    	DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
    	foreach ($tables as $table) {
    		DB::table($table)->truncate();
    	}
    	DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }
}
