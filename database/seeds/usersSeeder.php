<?php

use Illuminate\Database\Seeder;

use App\User;

class usersSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		User::create([
			'name' => 'Rick Sanchez',
			'email' => 'sanchez@reto.uno',
			'password' => bcrypt('secret')
		]);

		User::create([
			'name' => 'Morty Smith',
			'email' => 'smith@reto.uno',
			'password' => bcrypt('secret')
		]);
	}
}

/*
Morty Smith 	smith@reto.uno
Rick Sanchez 	sanchez@reto.uno

password = laravel
*/