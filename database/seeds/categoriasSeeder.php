<?php

use Illuminate\Database\Seeder;

use App\Categoria;

class categoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

//		DB::table('categorias')->insert(['nombre' => 'Ficción']);
    	Categoria::create(['nombre' => 'Ficción']);
    	Categoria::create(['nombre' => 'Ciencia']);
    	Categoria::create(['nombre' => 'Filosofía']);
    }
}

/*
Ficción
Ciencia
Filosofía
*/