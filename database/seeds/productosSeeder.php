<?php

use Illuminate\Database\Seeder;

use App\Producto;
use App\Categoria;

class productosSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		//SQL:	$catFiccion = DB::table('categorias')->where('nombre', 'Ficción')->value('id');
		$catFiccion = Categoria::where('nombre', 'Ficción')->value('id');
		$catCiencia = Categoria::where('nombre', 'Ciencia')->value('id');
		$catFilosofia = Categoria::where('nombre', 'Filosofía')->value('id');

		// datos =

		Producto::create(['categorias_id' => $catFiccion,
			'nombre' => 'Fundación, Isaac Asimov.',
			'precio' => 20 ]);
		Producto::create(['categorias_id' => $catFiccion,
			'nombre' => 'Un mundo feliz, Aldous Huxley.',
			'precio' => 20 ]);
		Producto::create(['categorias_id' => $catFiccion,
			'nombre' => 'Starship Troopers, Robert A. Heinlein.',
			'precio' => 20 ]);
		Producto::create(['categorias_id' => $catFiccion,
			'nombre' => 'Pórtico, Frederik Pohl.',
			'precio' => 20 ]);
		Producto::create(['categorias_id' => $catFiccion,
			'nombre' => 'Crónicas Marcianas, Ray Bradbury.',
			'precio' => 20 ]);

		Producto::create(['categorias_id' => $catCiencia,
			'nombre' => 'El futuro de nuestra mente, Michio Kaku.',
			'precio' => 20 ]);
		Producto::create(['categorias_id' => $catCiencia,
			'nombre' => 'Breve historia de mi vida, Stephen Hawking.',
			'precio' => 20 ]);
		Producto::create(['categorias_id' => $catCiencia,
			'nombre' => 'Somos nuestro cerebro: cómo pensamos, sufrimos y amamos, Dick Swaab.',
			'precio' => 20 ]);
		Producto::create(['categorias_id' => $catCiencia,
			'nombre' => 'La cuenta atrás, Alan Weisman.',
			'precio' => 20 ]);
		Producto::create(['categorias_id' => $catCiencia,
			'nombre' => 'Preguntas al aire: la meteorología tiene la respuesta, José Miguel Viñas.',
			'precio' => 20 ]);

		Producto::create(['categorias_id' => $catFilosofia,
			'nombre' => 'Tao Te Ching, de Lao Tsé.',
			'precio' => 20 ]);
		Producto::create(['categorias_id' => $catFilosofia,
			'nombre' => 'El Banquete, de Platón.',
			'precio' => 20 ]);
		Producto::create(['categorias_id' => $catFilosofia,
			'nombre' => 'Ética a Nicómaco, de Aristóteles.',
			'precio' => 20 ]);
		Producto::create(['categorias_id' => $catFilosofia,
			'nombre' => 'El discurso del método, de Descartes.',
			'precio' => 20 ]);
		Producto::create(['categorias_id' => $catFilosofia,
			'nombre' => 'Investigación sobre el entendimiento humano, de David Hume.',
			'precio' => 20 ]);

	}
}

/*
Fundación, Isaac Asimov.
Un mundo feliz, Aldous Huxley.
Starship Troopers, Robert A. Heinlein.
Pórtico, Frederik Pohl.
Crónicas Marcianas, Ray Bradbury.

El futuro de nuestra mente, Michio Kaku.
Breve historia de mi vida, Stephen Hawking.
Somos nuestro cerebro: cómo pensamos, sufrimos y amamos, Dick Swaab.
La cuenta atrás, Alan Weisman.
Preguntas al aire: la meteorología tiene la respuesta, José Miguel Viñas.

Tao Te Ching, de Lao Tsé.
El Banquete, de Platón.
Ética a Nicómaco, de Aristóteles.
El discurso del método, de Descartes.
Investigación sobre el entendimiento humano, de David Hume.
*/