@extends('layout')

@section('title', "Usuarios")

@section('content')

	<h1>Usuarios</h1>

	<ul>
		@forelse ($users as $user)
			<li> {{ $user->name }} <a href=" {{ route('showusuarios' , ['id' => $user->id]) }} "> Ver </a></li>
		@empty 
			<p>No hay usuarios registrados.</p>
		@endforelse
	</ul>

@endsection