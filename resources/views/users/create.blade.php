@extends('layout')

@section('title', "Registro de usuarios")

@section('content')

	<style>
		.in-64 {
    	float: right;
    	width: 64%;
    	height: 28px;
		}
	</style>
	<h1>Crear usuario</h1>
	<div class="row">
	<div class="col-sm-6 col-lg-4">
	<form method="POST" action="{{ route('setusuarios') }}" id="ref-form" >
		{{ csrf_field() }}
		<label for="name">Nombre </label>
		<input class="in-64" type="text" name="name" id="name" value="{{ old('name') }}" placeholder="Tu nombre" >
		<br>
		<label for="email">Email </label>
		<input class="in-64" type="email" name="email" id="email" value="{{ old('email') }}" placeholder="corre@reto.uno">
		<br>
		<label for="password">Contraseña </label>
		<input class="in-64" type="password" name="password" id="password" placeholder="Minimo 6 caracteres" >
		<br>
		<br>
	</form>
	</div>
	<div class="col-sm-6 col-md-4">
		@if ($errors->any())
			<div class="alert alert-info" role="alert">
				@foreach ($errors->all() as $e)
					<span>{{ $e }}</span><br>
				@endforeach
			</div>
		@endif
	</div>
	</div>
		<button class="btn btn-success" type="submit" form="ref-form" >Crear usuario</button>

	<br><br>
	@if ( auth()->user() != null ) 
	<a href="{{ route('usuarios') }}">Ir a Usuarios</a>
	@endif

@endsection