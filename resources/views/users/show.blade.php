@extends('layout')

@section('title', "{$user->name}")

@section('content')	

	@if ($user)
		<h1>{{$user->name}}</h1>
		<h3>{{$user->email}}</h3>
	@else
		<h1>Usuario no existe</h1>	
	@endif
	<br>
	<p><a href="{{ route('usuarios') }}">Regresar</a></p>

@endsection