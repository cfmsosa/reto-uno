@extends('layout')

@section('title', "Editar productos")

@section('content')

	<style>
		.in-64 {
    	float: right;
    	width: 64%;
    	height: 28px;
		}
	</style>

	<h1>Editar producto</h1>
	<div class="row">
	<div class="col-sm-6 col-lg-4">
	<form method="POST" action="{{ route('updateproductos',['producto' => $producto]) }}" id="ref-form" >
		{{ method_field('PUT') }}
		{{ csrf_field() }}
		<label for="nombre">Nombre </label>
		<input class="in-64" type="text" name="nombre" id="nombre" placeholder="" value="{{ old('nombre' , $producto->nombre) }}">
		<br>

		<?php $i = $producto->categorias_id ; ?>
		<label for="categorias">Categoria </label>
		<select class="in-64" name="categorias_id" id="categorias_id" form="ref-form" >
			@foreach ($categorias as $categoria)
				@if ( $categoria->id == $i)
				<option selected="selected" value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>
				@else
				<option value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>
				@endif
			@endforeach
		</select>
		<br>

		<label for="precio">Precio </label>
		<input class="in-64" type="number" min="0" name="precio" id="precio" value="{{ old('nombre' , $producto->precio) }}" placeholder="" >
		<br>

		<label for="descripcion">Descripcion </label>
		<input class="in-64" type="text" name="descripcion" id="descripcion" value="{{ old('descripcion' , $producto->descripcion) }}" placeholder="" >
		<br><br>
	</form>
	</div>
	<div class="col-sm-6 col-md-4">
		@if ($errors->any())
			<div class="alert alert-info" role="alert">
				@foreach ($errors->all() as $e)
					<span>{{ $e }}</span><br>
				@endforeach
			</div>
		@endif
	</div>
	</div>
		<button class="btn btn-success" type="submit" form="ref-form" >Editar Producto</button>

	<br><br>
	<a href=" {{ route('singleproductos' , ['id' => $producto->id]) }} ">Regresar</a>

@endsection