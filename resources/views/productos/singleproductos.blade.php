@extends('layout')

@section('title', "Producto {$producto->id}")

@section('content')
	<style>
		.col-be {
		    height: 60px;
		}
		.be {
			position: absolute;
			top: 12px;
			right: 15px;
		}
		h1 {
			min-height: 95px;
		}
	</style>
	<div class="row">
		<div class="col-sm-10">
			<h1>{{$producto->nombre}} </h1>
		</div>
		<div class="col-sm-2 col-be">
				<a class="btn btn-info be" href="{{ route('editproductos',['producto' => $producto]) }}">Editar</a>
		</div>
	</div>
	@if ($producto)
		<h2 class="text-secondary float-right">{{$categoria->nombre}}</h2>
		<h2 class="text-info" >Precio = {{$producto->precio}}</h2>
		<h4>{{$producto->descripcion}}</h4>
	@else
		<h1>Producto no existe</h1>	
	@endif
	<br>
	<a href="{{ route('showproductos' , ['id' => $producto->categorias_id]) }}">Libros de {{$categoria->nombre}}</a>

@endsection