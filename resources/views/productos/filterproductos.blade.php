@extends('layout')

@section('title', "Filtrar productos")

@section('content')

	<h1>Resultados de la busqueda: </h1>

	<ul>
		@forelse ($productos as $producto)
			<li> 
				<a href=" {{ route('singleproductos' , ['id' => $producto->id]) }} "> {{ $producto->nombre }}</a>
			</li>
		@empty 
			<p>No hay productos de esta categoria.</p>
		@endforelse
	</ul>

	<p><a href="{{ route('showcategorias') }}">Regresar</a></p>

@endsection