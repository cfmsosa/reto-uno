@extends('layout')

@section('title', "Productos de {$categoria->nombre}")

@section('content')

	<h1>Productos de la categoría {{ $categoria->nombre }} </h1>

	<ul>
		@forelse ($productos as $producto)
			<li> 
				<a href=" {{ route('singleproductos' , ['id' => $producto->id]) }} "> {{ $producto->nombre }}</a>
			</li>
		@empty 
			<p>No hay productos de esta categoria.</p>
		@endforelse
	</ul>

	<p><a href="{{ route('showcategorias') }}">Regresar</a></p>

@endsection