@extends('layout')

@section('title', "Editar categorias")

@section('content')

	<style>
		.in-64 {
    	float: right;
    	width: 64%;
    	height: 28px;
		}
	</style>

	<h1>Editar categoria</h1>
	<div class="row">
	<div class="col-sm-6 col-lg-4">

	<form method="POST" action="{{ route('updatecategorias',['id' => $categoria->id]) }}" id="ref-form" >
		{{ method_field('PUT') }}
		{{ csrf_field() }}

		<label for="nombre">Nombre </label>
			<input class="in-64" type="text" name="nombre" id="nombre" value="{{ old('nombre' , $categoria->nombre) }}"  >
		<br>
		<br>
	</form>
	</div>
	<div class="col-sm-6 col-md-4">
		@if ($errors->any())
			<div class="alert alert-info" role="alert">
				@foreach ($errors->all() as $e)
					<span>{{ $e }}</span><br>
				@endforeach
			</div>
		@endif
	</div>
	</div>
		<button class="btn btn-success" type="submit" form="ref-form" >Editar categoria</button>

	<br><br>
	<p><a href="{{ route('showcategorias') }}">Ir a Categorias</a></p>

@endsection