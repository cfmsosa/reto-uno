@extends('layout')

@section('title', "Categorias")

@section('content')
	<style>
		.top {
			position: relative;
		}
		.dropdown {
			position: absolute;
			bottom: 0;
			right: 0;
		}
	</style>
<div class="row">
	<div class="col-sm-8 col-md-6 col-xl-4">
		<div class="top">
			<h1>Categorias</h1>
			<div class="dropdown">
				<button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Editar
				</button>
				<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
					@foreach ($categorias as $categoria)
						<a class="dropdown-item" href="{{ route('editcategorias',['id' => $categoria->id]) }}"">
							{{ $categoria->nombre }}
						</a>
					@endforeach
					<div class="dropdown-divider"></div>
					<a class="dropdown-item font-weight-bold" href="{{ route('createcategorias') }}"">
						Nueva categoría
					</a>
				</div>
			</div>
		</div>
		<div>
		<ul>
			@foreach ($categorias as $categoria)
				<li>
				<a href=" {{ route('showproductos' , ['id' => $categoria->id]) }} ">{{ $categoria->nombre }}</a>
				</li>
			@endforeach
		</ul>	
		</div>
	</div>
</div>

@endsection