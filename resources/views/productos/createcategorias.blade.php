@extends('layout')

@section('title', "Crear categorias")

@section('content')

	<style>
		.in-64 {
    	float: right;
    	width: 64%;
    	height: 28px;
		}
	</style>

	<h1>Crear categoria</h1>
	<div class="row">
	<div class="col-sm-6 col-lg-4">
	<form method="POST" action="{{ route('setcategorias') }}" id="ref-form" >
		{{ csrf_field() }}
		<label for="nombre">Nombre </label>
		<input class="in-64" type="text" name="nombre" id="nombre" value="{{ old('nombre') }}" placeholder="" >
		<br>
		<br>
	</form>
	</div>
	<div class="col-sm-6 col-md-4">
		@if ($errors->any())
			<div class="alert alert-info" role="alert">
				@foreach ($errors->all() as $e)
					<span>{{ $e }}</span><br>
				@endforeach
			</div>
		@endif
	</div>
	</div>
		<button class="btn btn-success" type="submit" form="ref-form" >Crear categoria</button>

	<br><br>
	<p><a href="{{ route('showcategorias') }}">Ir a Categorias</a></p>

@endsection