<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Reto Uno</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>

<body>

<header>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-light bg-info">
        <a class="navbar-brand text-light" href="{{ route('home') }}">Reto Uno</a>
        @if ( auth()->user() != null ) 
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item ">
                    <a class="nav-link" href="{{ route('usuarios') }}">Usuarios</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="{{ route('showcategorias') }}">Categorias</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="{{ route('createproductos') }}">Nuevo producto</a>
                </li>
            </ul>

                <!-- FILTER -->
                <ul class="nav justify-content-end">
                <form method="GET" action="{{ route('filterproductos') }}" id="find" >
                    {{ csrf_field() }}
                    <div class="input-group">
                    <input type="text" name="campo" id="campo" class="form-control form-control-sm" required="required" >
                    <div class="input-group-apend">
                    <input class="btn bg-light btn-sm btn-outline-secondary" type="submit" name="submit" form="find" value="Buscar">
                    </div>
                    </div>
                </form>

                <!-- LOG OUT -->
                <form method="POST" action="{{ route('logout') }}" id="logout" >
                    {{ csrf_field() }}
                    <input class="btn bg-warning btn-sm ml-2" type="submit" name="submit" form="logout" value="Cerrar sesión">
                </form>
                </ul>

        </div>
        @endif
    </nav>
</header>

<!-- Begin page content -->
<main role="main" class="container">
    <div class="row mt-3">
        <div class="col-12">
            @yield('content')
        </div>
    </div>
</main>

<footer class="footer">
</footer>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>
</html>