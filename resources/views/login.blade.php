@extends('layout')

@section('title', "Registro de usuarios")

@section('content')

            <div class="col-md-4 mx-auto" >
                <form method="POST" action="{{ Route('login') }}">
                    <div class="form-group" >
                    {{ csrf_field() }}
                    <label for="email" >Email</label>
                    <input type="email" name="email" placeholder="Email" class="form-control">
                    {!! $errors->first('email','<span class="help-block text-danger" >:message</span>') !!}
                    </div>
                    <div class="form-group" >
                    <label for="password" >Contraseña</label>
                    <input type="password" name="password" class="form-control">
                    {!! $errors->first('password','<span class="help-block text-danger" >:message</span>') !!}
                    </div>
                    <br>
                    <button class="btn btn-success btn-block" >Iniciar Sesión</button>
                </form>
                <p class="text-center mt-3" >
                    <a href="{{ route('createusuarios') }}">Registrar nuevo usuario</a>
                </p>
                
            </div>
        
@endsection